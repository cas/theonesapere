package extension;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import one.core.Application;
import one.core.Connection;
import one.core.DTNHost;
import one.core.Message;
import one.core.Settings;
import sapere.lsa.Id;
import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.node.NodeManager;
import sapere.node.lsaspace.Operation;

/**
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 */
public abstract class AbstractSapereHost extends Application {

	/**
	 * The application ID for a sapere host in The One.
	 */
	public static final String APP_ID = "SapereHost";

	/**
	 * The node manager.
	 */
	private NodeManager my_nodeManager;

	boolean my_executionSwitch = true;

	/**
	 * The map containing the neighbouring nodes and their LSAs
	 */
	private Map<String, Lsa> my_neighbourLSAs = new HashMap<String, Lsa>();

	/**
	 * The node hosting the application.
	 */
	private DTNHost my_host;

	/**
	 * Whether we have setup the application or not.
	 */
	private boolean my_initialised;

	/**
	 * The application settings.
	 */
	private final Settings my_settings;

	/**
	 * Creates a new SAPERE host.
	 * 
	 * @param some_settings
	 *            the settings file containing application settings.
	 */
	public AbstractSapereHost(final Settings some_settings) {
		super();
		my_settings = some_settings;
		super.setAppID(APP_ID);
	}

	/**
	 * Extracts the LSA from any received message and passes it to the
	 * NetworkReceiverManager for injection into the local LSA Space.
	 * 
	 * @param a_message
	 *            the received message.
	 * @param the_localHost
	 *            the ? host.
	 * @return this method always returns null to indicate that the message
	 *         should be dropped. As direct routing is the only type allowed,
	 *         this should always result in the correct behaviour (i.e.,
	 *         matching the middleware).
	 */
	@Override
	public Message handle(final Message a_message, DTNHost the_localHost) {
		// System.err.println(" From:" + a_message.getFrom().getName() + " To:"
		// +
		// a_message.getTo().getName() + " "
		// + a_message.getProperty("lsa"));
		Lsa receivedLsa = (Lsa) a_message.getProperty("lsa");
		if (!a_message.getTo().getName().equals(the_localHost.getName()))
			return null;
		if (receivedLsa != null) {

			// my_nodeManager.getNetworkReceiverManager().doInject(receivedLsa);
			my_nodeManager.getNetworkReceiverManager().onLsaReceived(receivedLsa);

		}
		return null;
	}

	/**
	 * A delegate method that calls the operation manager, eco-law engine on
	 * alternate cycles, and calls implementing classes on each cycle.
	 */
	public void sapereUpdate(final DTNHost host) {

		// Update the neighbouring space LSAs
		updateNeighbours(host);

		// Call subclasses
		onSimulationCycle();

		// Run the middleware engines
		my_nodeManager.getOperationManager().run();
		my_nodeManager.getEcoLawsEngine().run();
	}

	/**
	 * A delegate method that calls the operation manager, eco-law engine on
	 * alternate cycles, and calls implementing classes on each cycle.
	 */
	@Override
	public void update(final DTNHost host) {
		// Performs the initial initialisation. Otherwise we use the
		// sapereUpdate() method.
		if (!my_initialised) {
			initialiseHost(host);
			my_initialised = true;
		}

		// if (SapereProperties.CYCLE_COUNTER % 200 == 0){
		// Class<? extends AbstractSapereHost> hostClass = getClass();
		// String hostName = getHost().getName();
		// try {
		// final Method setupMethod = hostClass.getMethod("setup" + hostName);
		// setupMethod.invoke(this);
		// //System.out.println(hostName + " setup complete");
		// } catch (Exception e) {
		// // The setup method doesn't exist, so we skip over it.
		// //System.out.println("No individual setup specified for "+ hostName);
		// }
		// }
	}

	/**
	 * @param host
	 */
	private void initialiseHost(DTNHost a_host) {
		my_host = a_host;
		// Create the node manager
		my_nodeManager = new NodeManager(a_host, my_settings);
		onSetup();
	}

	/**
	 * Perform setup operations, like injecting an LSA or stating an agent. This
	 * method used reflection trickery to find methods in implementing classes
	 * corresponding to the node name.
	 */
	public final void onSetup() {

		// Get a reference to the class and host name
		Class<? extends AbstractSapereHost> hostClass = getClass();
		String hostName = getHost().getName();

		// 1. Everyone
		try {
			final Method setupMethod = hostClass.getMethod("setupAll");
			setupMethod.invoke(this);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		// 2. Single node
		try {
			final Method setupMethod = hostClass.getMethod("setup" + hostName);
			setupMethod.invoke(this);
			// System.out.println(hostName + " setup complete");
		} catch (Exception e) {
			// The setup method doesn't exist, so we skip over it.
			// System.out.println("No individual setup specified for "+
			// hostName);
		}

		// added by Francesco De Angelis
		// 3. Single group
		//
		try {

			String groupId = getHost().getGroupId();

			final Method setupMethod = hostClass.getMethod("setupAll" + groupId, String.class);
			setupMethod.invoke(this, hostName);
		} catch (Exception e) {
			// e.printStackTrace();
		}
	}

	/**
	 * Perform operations during the simulation cycle
	 */
	public abstract void onSimulationCycle();

	/**
	 * Makes the GUI visible.
	 */
	public void setConsoleVisible() {
		if (!(my_nodeManager == null)) {
			this.my_nodeManager.setConsoleVisible();
		}
	}

	/**
	 * Gets the NodeManager for this Sapere host.
	 * 
	 * @return the node manager.
	 */
	public NodeManager getNodeManager() {
		return my_nodeManager;
	}

	/**
	 * Updates all the neighbour LSAs in the host based on the simulator state.
	 * 
	 * @param host
	 */
	private void updateNeighbours(final DTNHost host) {
		// Create the list of nodes this node is connected to.
		final ArrayList<String> neighbouringNodeNameList = new ArrayList<String>();
		final List<Connection> connectionList = host.getConnections();
		for (final Connection connection : connectionList) {
			neighbouringNodeNameList.add(connection.getOtherNode(host).getName());
		}

		// Create LSAs for each *new* neighbour
		for (final String neighbourName : neighbouringNodeNameList) {
			if (!my_neighbourLSAs.containsKey(neighbourName)) {
				final Lsa lsa = new Lsa();
				// lsa.addProperty(new Property("name", neighbourName));
				lsa.addProperty(new Property("neighbour", neighbourName));
				final Operation operation = new Operation().injectOperation(lsa, "neighbour_lsa_agent", null);
				Id id = my_nodeManager.getOperationManager().execOperation(operation);
				lsa.setId(id);
				my_neighbourLSAs.put(neighbourName, lsa);
			}
		}

		// Remove LSAs for neighbours no longer connected.
		final List<String> departedNeigbours = new ArrayList<String>();

		for (Map.Entry<String, Lsa> entry : my_neighbourLSAs.entrySet()) {
			if (!neighbouringNodeNameList.contains(entry.getKey())) {
				final Operation operation = new Operation().removeOperation(entry.getValue().getId(),
						"neighbour_lsa_agent");
				my_nodeManager.getOperationManager().execOperation(operation);
				departedNeigbours.add(entry.getKey());
			}
		}

		for (String removeName : departedNeigbours) {
			my_neighbourLSAs.remove(removeName);
		}
	}

	/**
	 * Return the node hosting the application.
	 * 
	 * @return the host node.
	 */
	public DTNHost getHost() {
		return my_host;
	}

	/**
	 * Reruns the application settings file.
	 * 
	 * @return the settings.
	 */
	public Settings getSettings() {
		return my_settings;
	}

}
