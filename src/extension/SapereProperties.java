package extension;

import java.awt.Color;
import java.util.Random;

public class SapereProperties {
	
	public static boolean PRINT_GRADIENT = true;
	/**
	 * An enum representing the different spreading modes
	 */
	public static enum Spreading {
		REGULAR, PROBABALISTIC, ADAPTIVE_PROBABALISITC, LOCATION_BASED, MIDDLEWARE_STANDARD, SWITCHING
	};
	
	public static int DEFAULT_GRADIENT_MAX_HOP = 10;
	
	//Counters for metrics.
	public static int MESSAGE_COUNTER = 0;
	
	//If true we assume real broadcast communication. 
	//The metrics coming from The ONE will be wrong. 
	public static boolean BROADCAST = true;
	
	//Chances of a node haveing GPS enabled.
	public static final double GPS_RATIO = 0.5f;
	
	// No longer required. 
	// Use simulator properties to configure eco law firing frequency
	public static int CYCLE_COUNTER = 0;
	
	// frequency invocation eco-laws
	public static float ECOLAW_FREQ = 30; //cycles number
	// frequency invocation direct propagation
	public static float ECOLAW_PROP_FREQ = 30; //cycles number
	// frequency invocation gradient propagation
	public static float ECOLAW_GRADIENT_FREQ = 30;
	
	public static boolean BATCH_MODE = false;
	
	public static long SEED = 2;
	
	public static Random RANDOM = new Random(SEED);
	
	public static int RUN_NUMBER = 1;
	
	public static int location = 0;
	public static int prob = 0;
	
	public static final boolean QUIT_IF_SEGMENTED = false;
	
	public static boolean GRADIENTS_EVALUATION = true;
	
	/**
	 * The type of spreading to enable
	 */
	public static final Spreading SPREADING_MODE = Spreading.REGULAR;

	/**
	 * Whether to repeatedly propagate seen messages.
	 */
	public static final boolean PROPAGATE_ONCE_ONLY = true;
	public static int NODES_NUMBER = 70;
	// decay value before respreading the gradient
	public static String GRADIENT_DECAY_VALUE = "60";
	public static int CHEMO_DECAY_VALUE = 40;
	
	public static enum ChemoPath {
		CHEMO_1, CHEMO_2
	};
	public static String NODE_READER_1 = "Node25";
	public static String NODE_READER_2 = "Node37";	
	public static String NODE_INJECTOR = "Node0";
	public static Color COLOR_PATH_1 = Color.orange;
	public static Color COLOR_PATH_2 = Color.CYAN;
	public static int CHEMO_POINT_SIZE = 4;
	public static int CHEMO_LINE_SIZE = 5;
	
	/**
	 * The minimum probability threshold to be used.
	 */
	public static final float MINIMUM_PROBABILITY = 0.8f;
	
	/**
	 * Alpha changes the slope of the function that provides the adaptive Prob.
	 */
	public static int ALPHA = 8;
	/**
	 * The spreading probability
	 */
	public static float SPREADING_PROBABILITY = 0.88f;
	
	
	public static int LOCATION_THRESHOLD = 40;

	// Smaller map?
	public static final boolean SMALLER_MAP = false;

	// Limit of the smaller maps
//	public static double MAX_X_BOUND = 2552500;
//	public static double MAX_Y_BOUND = 6672600;
//
//	public static double MIN_X_BOUND = 2552400;
//	public static double MIN_Y_BOUND = 6672500;
	
	public static double MAX_X_BOUND = 4552500/2;
	public static double MAX_Y_BOUND = 8672600/2;

	public static double MIN_X_BOUND = 4552400/2;
	public static double MIN_Y_BOUND = 8672500/2;

	// SOME MATHS FOR REDUCING THE MAP

	// MAX AND MIN GPS POSITIONS
	// MAX_X = 2554569
	// MIN_X = 2550186

	// MAX_Y = 6674388
	// MIN_Y = 6671039

	// SIZE OF THE MAP IN GPS units
	// MAX_X - MIN_X = 4383
	// MAX_Y - MIN_Y = 3349

	// I am going to remove 1000 from each side.

	// public static double MAX_X_BOUND = 2553569;
	// public static double MAX_Y_BOUND = 6673388;

	// public static double MIN_X_BOUND = 2551186;
	// public static double MIN_Y_BOUND = 6672039;
	
	
	//Metrics for Chemotaxis.
	public static int hops = 0; // for counting the number of hops with Chemotaxis
	public static int messagesCreated = 0; //messeges that are created to follow the gradient
	public static int messageDelivered = 0; //Messages that are delivered to node 0. 
	

}
