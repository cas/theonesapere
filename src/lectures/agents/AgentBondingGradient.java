package lectures.agents;

/*
 * This is an agent that creates a custom property in its
 * LSA to trigger bond notification with LSAs created by agents 
 * of class AgentGradient. It can be used to read values coming from 
 * other nodes of the network.
 */
import one.core.DTNHost;
import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.lsa.interfaces.ILsa;
import sapere.lsa.values.PropertyName;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

public class AgentBondingGradient extends SapereAgent {

   private final String agentName;
   private DTNHost myHost;
   /**
    * Creates a new agent.
    * @param name the name of the node
    * @param an_opMng the operation manager for the node.
    */
   public AgentBondingGradient(String name, NodeManager a_nodeManager) {
      super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
      agentName = name;
      myHost = a_nodeManager.getHost();
   }

   /**
    * {@inheritDoc}
    */
   public void onBondAddedNotification(BondAddedEvent event)
   {
	   // get the reference to the bound LSA
	   ILsa boundLsa = event.getBondedLsa();
	   // read the property of the bound LSA
	   Property prop = boundLsa.getProperty("propertyName");
	   // this is the property value that has been spread
	   String value = prop.getValue().firstElement();

	   // we get also the hop counter value of the gradient
	   //
	   Property graProp = boundLsa.getProperty(PropertyName.GRADIENT_HOP.toString());  
	   String grValue = graProp.getValue().firstElement();
	   int gr = Integer.parseInt(grValue);
	   
	   // we update the hop counter for this specific gradient id 
	   // NOTE: we do it to visualize it in the simulator.
	   graProp = boundLsa.getProperty(PropertyName.GRADIENT.toString());  
	   String gId = graProp.getValue().firstElement();
	   
	   this.myHost.insertGradientValue(gId, gr);
	   //System.out.println(this.agentName + " gValue: " + myHost.gValue);
	   System.out.println(this.agentName + " property read: " + value + " g=" + grValue);
   }

   /**
    * {@inheritDoc}
    */
   public void onBondRemovedNotification(BondRemovedEvent event) 
   {
	   String gValue = event.getCopyRemovedLsa().getProperty(PropertyName.GRADIENT_HOP.toString()).getValue().firstElement();
	   
	   System.out.println(this.agentName + " removed old gradient " + gValue);
   }

   /**
    * {@inheritDoc}
    */ 
   public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) 
   {
   }

   /**
    * {@inheritDoc}
    */
   public void onPropagationEvent(PropagationEvent event) {
   }

   /**
    * {@inheritDoc}
    */
   public void setInitialLSA() {
	   System.out.println("Created agent " + agentName ); 
	   
	   // we want to read a value placed in a property named "propertyName"
	   addSubDescription("sub", new Property("propertyName","*"));

	   // NOTE: do not forget to invoke this method to 
	   // update the LSA of the agent
	   submitOperation();
   }

   public void onReadNotification(ReadEvent readEvent) {
	// TODO Auto-generated method stub
   }

   public void onDecayedNotification(DecayedEvent event) {
	// TODO Auto-generated method stub
   }
}