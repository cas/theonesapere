package lectures.agents;

/*
 * This is an agent that uses aggregation to calculate the value 
 * of its LSA automatically by using an aggregation function applied 
 * on the values of the LSAs of agents of class AgentAggregationInsertValue.
 */
import sapere.agent.SapereAgent;
import sapere.lsa.values.AggregationOperators;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

public class AgentAggregation extends SapereAgent {

	private final String agentName;

	/**
	 * Creates a new agent.
	 * 
	 * @param name
	 *            the name of the node
	 * @param an_opMng
	 *            the operation manager for the node.
	 */
	public AgentAggregation(String name, NodeManager a_nodeManager) {
		super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
		agentName = name;
	}

	/**
	 * {@inheritDoc}
	 */
	public void onBondAddedNotification(BondAddedEvent event) {
	}

	/**
	 * {@inheritDoc}
	 */
	public void onBondRemovedNotification(BondRemovedEvent event) {
	}

	/**
	 * {@inheritDoc}
	 */
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
	}

	/**
	 * {@inheritDoc}
	 */
	public void onPropagationEvent(PropagationEvent event) {
	}

	/**
	 * {@inheritDoc}
	 */
	public void setInitialLSA() {
		System.out.println("Created agent " + agentName);

		// look at: sapere.node.lsaspace.ecolaws.AggregationOperators
		// to see the list of aggregation operators
		// NOTE: this property value will be automatically computed
		// depending on the values of the LSA of the agents concurring to
		// the aggregation process.
		addOtherAggregation("aggregationProperty", AggregationOperators.MAX.toString());
		// NOTE: do not forget to invoke this method to
		// update the LSA of the agent
		submitOperation();
	}

	public void onReadNotification(ReadEvent readEvent) {
		// TODO Auto-generated method stub
		System.out.println("Read event " + agentName);

	}

	public void onDecayedNotification(DecayedEvent event) {
		// TODO Auto-generated method stub
	}
}