package lectures.agents;

/*
 * This is an agent that uses spreading to spread 
 * its LSA in the LSA spaces of its neighbors.
 */
import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

public class AgentSpreading extends SapereAgent {

   private final String agentName;
   private String value;

   /**
    * Creates a new agent.
    * @param name the name of the node
    * @param an_opMng the operation manager for the node.
    * @param value user-data information used as payload.
    */
   public AgentSpreading(String name, NodeManager a_nodeManager, String value) {
      super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
      agentName = name;
      this.value = value;
   }

   /**
    * {@inheritDoc}
    */
   public void onBondAddedNotification(BondAddedEvent event)
   {
   }

   /**
    * {@inheritDoc}
    */
   public void onBondRemovedNotification(BondRemovedEvent event) 
   {
   }

   /**
    * {@inheritDoc}
    */ 
   public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) 
   {
   }

   /**
    * {@inheritDoc}
    */
   public void onPropagationEvent(PropagationEvent event) {
	   System.out.println("Sent!");
   }

   /**
    * {@inheritDoc}
    */
   public void setInitialLSA() {
	   System.out.println("Created agent " + agentName );
	   
	   // we add a property with a generic value
	   addProperty(new Property("propertyName",value));
	   
	   // we spread it.
	   // NOTE: try addDirectPropagationOnce,
	   //			addDirectPropagation
	   //			addDirectPropagationOnce(destination)
	   //			addDirectPropagation(destination)
	   addDirectPropagation("B3");
	   
	   // NOTE: do not forget to invoke this method to 
	   // update the LSA of the agent
	   submitOperation();
   }

   public void onReadNotification(ReadEvent readEvent) {
	// TODO Auto-generated method stub
   }

   public void onDecayedNotification(DecayedEvent event) {
	// TODO Auto-generated method stub
   }
}