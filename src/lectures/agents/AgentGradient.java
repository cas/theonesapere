package lectures.agents;

/*
 * This agent used the dynamic gradient to spread its LSA all over 
 * the network. The LSA carries some user data used as payload. 
 */


import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

public class AgentGradient extends SapereAgent {

   private final String agentName;
   private String value;
   private int numHops;
   private NodeManager nm;
   
   // the number of gradients generated
   private int gradientId=0;
   
   /**
    * Creates a new agent.
    * @param name the name of the node
    * @param an_opMng the operation manager for the node.
    * @param value user data information used as payload.
    * @param hop max number of hops that a gradient message can visit before being deleted.
    */
   public AgentGradient(String name, NodeManager a_nodeManager, String value, int hops) {
      super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
      agentName = name;
      this.value = value;
      this.numHops = hops;
      this.nm = a_nodeManager;
      
   }

   /**
    * {@inheritDoc}
    */
   public void onBondAddedNotification(BondAddedEvent event)
   {
   }

   /**
    * {@inheritDoc}
    */
   public void onBondRemovedNotification(BondRemovedEvent event) 
   {
   }

   /**
    * {@inheritDoc}
    */ 
   public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) 
   {
   }

   /**
    * {@inheritDoc}
    */
   public void onPropagationEvent(PropagationEvent event) {
	   //System.out.println("Sent!");
   }

   /**
    * {@inheritDoc}
    */
   public void setInitialLSA() {
	  System.out.println("Created agent " + agentName );
	   
	  addProperty(new Property("propertyName",value));
	  
	  // we create a new gradient id with specific expire time
	  addDynamicGradient("G"+(gradientId++), 3000);
	   // NOTE: do not forget to invoke this method to 
	   // update the LSA of the agent
	   submitOperation();
   }

   public void onReadNotification(ReadEvent readEvent) {
	// TODO Auto-generated method stub
   }

   public void onDecayedNotification(DecayedEvent event) {
	   
	   this.resetId();
	   // we track the decay event associated with the gradient
	   // and we inject a new gradient message
	   setInitialLSA();

   }
}