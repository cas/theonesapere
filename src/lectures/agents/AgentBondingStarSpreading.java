package lectures.agents;

/*
 * This is an agent that creates a custom property in its
 * LSA to trigger bond notification with LSA that are spread by agents 
 * of class AgentSpreading. It can be used to read values coming from 
 * other nodes of the network.
 */
import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.lsa.interfaces.ILsa;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

public class AgentBondingStarSpreading extends SapereAgent {

   private final String agentName;

   /**
    * Creates a new agent.
    * @param name the name of the node
    * @param an_opMng the operation manager for the node.
    */
   public AgentBondingStarSpreading(String name, NodeManager a_nodeManager) {
      super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
      agentName = name;
   }

   /**
    * {@inheritDoc}
    */
   public void onBondAddedNotification(BondAddedEvent event)
   {
	   // get the reference to the bound LSA
	   ILsa boundLsa = event.getBondedLsa();
	   // read the property of the bound LSA
	   Property prop = boundLsa.getProperty("propertyName");
	   

	   if (prop == null)
		   return;
	   
	   // NOTE: each property contain 1 name and n>=1 values
	   // this means that the content of the property is a vector
	   // of n elements.
	   // we get the first value
	   String value = prop.getValue().firstElement();
	   
	   System.out.println(agentName + " read: " + value);
   }

   /**
    * {@inheritDoc}
    */
   public void onBondRemovedNotification(BondRemovedEvent event) 
   {
	   // get the reference to the removed LSA
	   // NOTE: this LSA does not exist anymore in the LSA space
	   // it is a temporary copy that will be deleted after
	   // this call to the method onBondRemovedNotification
	   ILsa boundLsa = event.getCopyRemovedLsa();
	   // read the property of the removed LSA
	   Property prop = boundLsa.getProperty("propertyName");
	   
	   String value = prop.getValue().firstElement();
	   
	   System.out.println("Property read: " + value);
   }

   /**
    * {@inheritDoc}
    */ 
   public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) 
   {
	// get the reference to the bound LSA
	   ILsa boundLsa = event.getLsa();
	   // read the decay property of the bound LSA
	   Property prop = boundLsa.getProperty("propertyName");
	   
	   // NOTE: each property contain 1 name and n>=1 values
	   // this means that the content of the property is a vector
	   // of n elements.
	   // we get the first value
	   String value = prop.getValue().firstElement();
	   
	   System.out.println(this.agentName + " updated property value: " + value);
   }

   /**
    * {@inheritDoc}
    */
   public void onPropagationEvent(PropagationEvent event) {
   }

   /**
    * {@inheritDoc}
    */
   public void setInitialLSA() {
	   System.out.println("Created agent " + agentName );
	   
	   // we create a property to trigger the bonding eco-law
	   // w.r.t. the LSA created by the AgentDecay
	   // NOTE: we have to put it in a subdescription!	   
	   addSubDescription("sub", new Property("propertyName","*"));
	   
	   // NOTE: do not forget to invoke this method to 
	   // update the LSA of the agent
	   submitOperation();
   }

   public void onReadNotification(ReadEvent readEvent) {
	// TODO Auto-generated method stub
   }

   public void onDecayedNotification(DecayedEvent event) {
	// TODO Auto-generated method stub
   }
}