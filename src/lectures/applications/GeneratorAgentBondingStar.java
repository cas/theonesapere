package lectures.applications;

/*
 * 
 * This class generates the SAPERE agents for the simulation
 * 
 */

import lectures.agents.AgentBondingStar;
import lectures.agents.AgentSingleProperty;
import one.core.Application;
import one.core.Settings;
import eu.sapere_project.wp3lib.application.SapereApplication;

public final class GeneratorAgentBondingStar extends SapereApplication {

	public GeneratorAgentBondingStar(final Settings some_settings) {
		super(some_settings);
	}

	public void setupA0() {

		AgentBondingStar agent = new AgentBondingStar("A0", "nodeName", getNodeManager());
		agent.setInitialLSA();

		AgentSingleProperty agent2 = new AgentSingleProperty("A02", "nodeName", "A02", getNodeManager());
		agent2.setInitialLSA();

		AgentSingleProperty agent3 = new AgentSingleProperty("A03", "nodeName", "A03", getNodeManager());
		agent3.setInitialLSA();

		AgentSingleProperty agent4 = new AgentSingleProperty("A04", "nodeName", "A04", getNodeManager());
		agent4.setInitialLSA();
	}

	public void setupAll() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Application replicate() {
		return new GeneratorAgentBondingStar(getSettings());
	}
}
