package lectures.applications;

/*
 * 
 * This class generates the SAPERE agents for the simulation
 * 
 */

import lectures.agents.AgentBondingQuestion;
import lectures.agents.AgentSingleProperty;
import one.core.Application;
import one.core.Settings;
import eu.sapere_project.wp3lib.application.SapereApplication;
public final class GeneratorAgentBondingQuestion extends SapereApplication {
	
	
	public GeneratorAgentBondingQuestion(final Settings some_settings) {
		super(some_settings);
	}

	public void setupA0() 
	{	
		
		AgentSingleProperty agent1 = new AgentSingleProperty("A1","nodeName","A1",getNodeManager());
		agent1.setInitialLSA();
		
		// agent that reads the property of agent1
		AgentBondingQuestion agent2 = new AgentBondingQuestion("A2", "nodeName", getNodeManager());
		agent2.setInitialLSA();
	}
	public void setupAll()
	{
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Application replicate() {
		return new GeneratorAgentBondingQuestion(getSettings());
	}
}
