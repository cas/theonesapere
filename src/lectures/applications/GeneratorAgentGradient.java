package lectures.applications;

/*
 * 
 * This class generates the SAPERE agents for the simulation
 * 
 */

import lectures.agents.AgentBondingGradient;
import lectures.agents.AgentGradient;
import one.core.Application;
import one.core.Settings;
import eu.sapere_project.wp3lib.application.SapereApplication;
public final class GeneratorAgentGradient extends SapereApplication {
	
	public GeneratorAgentGradient(final Settings some_settings) {
		super(some_settings);
	}

	public void setupA0() 
	{	
		AgentGradient agent = new AgentGradient("A0gradient", getNodeManager(),"here A0spreading!",10);
		agent.setInitialLSA();
	}
	
	public void setupAllB(String nodeName)
	{
		AgentBondingGradient agent = new AgentBondingGradient("receiver"+nodeName, getNodeManager());
		agent.setInitialLSA();
	}
	
	public void setupAllC(String nodeName)
	{
		AgentBondingGradient agent = new AgentBondingGradient("receiver"+nodeName, getNodeManager());
		agent.setInitialLSA();
	}
	
	public void setupAllD(String nodeName)
	{
		AgentBondingGradient agent = new AgentBondingGradient("receiver"+nodeName, getNodeManager());
		agent.setInitialLSA();
	}
	
	public void setupAllE(String nodeName)
	{
		AgentBondingGradient agent = new AgentBondingGradient("receiver"+nodeName, getNodeManager());
		agent.setInitialLSA();
	}
	
	public void setupAllF(String nodeName)
	{
		AgentBondingGradient agent = new AgentBondingGradient("receiver"+nodeName, getNodeManager());
		agent.setInitialLSA();
	}
	
	public void setupAll()
	{
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Application replicate() {
		return new GeneratorAgentGradient(getSettings());
	}
}
