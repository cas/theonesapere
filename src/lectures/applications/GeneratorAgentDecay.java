package lectures.applications;

/*
 * 
 * This class generates the SAPERE agents for the simulation
 * 
 */

import lectures.agents.AgentBondingQuestion;
import lectures.agents.AgentDecay;
import lectures.agents.AgentSingleProperty;
import one.core.Application;
import one.core.Settings;
import eu.sapere_project.wp3lib.application.SapereApplication;
public final class GeneratorAgentDecay extends SapereApplication {
	
	public GeneratorAgentDecay(final Settings some_settings) {
		super(some_settings);
	}

	public void setupA0() 
	{	
		AgentSingleProperty agent = new AgentSingleProperty("A0","nodeName","A0", getNodeManager());
		//agent.setInitialLSA();
	}
	
	public void setupA1()
	{
		AgentBondingQuestion agent = new AgentBondingQuestion("A0", "decay", getNodeManager());
		agent.setInitialLSA();
		
		AgentDecay agent2 = new AgentDecay("A12", getNodeManager());
		agent2.setInitialLSA();
	}
	
	public void setupAll()
	{
		
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Application replicate() {
		return new GeneratorAgentDecay(getSettings());
	}
}
