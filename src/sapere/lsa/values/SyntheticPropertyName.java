/**
 * 
 */
package sapere.lsa.values;

import java.util.EnumSet;



/**
 * Names for Synthetic Properties
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public enum SyntheticPropertyName {
	
   // Edit 18/10/12 - Graeme: reordered to support alphabetical enumeration
   BONDS("#bonds"),
   CREATION_TIME("#creationTime"),
   CREATOR_ID ("#creatorId"),
	LAST_MODIFIED("#lastModified"),
	LOCATION("#location");
	    
	private SyntheticPropertyName (final String text) {
        this.text = text;
    }

    private final String text;

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return text;
    }

    public static boolean isSyntheticProperty(String name){
    	return (name).equals(LAST_MODIFIED.toString());
    }
	
	
	public static SyntheticPropertyName getSyntheticPropertiesValues (String s){
		
		for(SyntheticPropertyName sp : EnumSet.allOf(SyntheticPropertyName.class)){
			if(sp.toString().equals(s))
				return sp;
		}
		return null;
	}
	
	};
