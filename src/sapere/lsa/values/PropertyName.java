/**
 * 
 */
package sapere.lsa.values;

/**
 * Well-known Properties
 * @author Gabriella Castelli (UNIMORE)
 * @authoer Francesco De Angelis (UNIGE)
 */
public enum PropertyName {
	
	// added by Francesco De Angelis
	// for gradient
	GRADIENT("gradient_diffusion"),
	GRADIENT_PREVIOUS("gradient_previous"),
	GRADIENT_PREVIOUS_LOCAL("local"),
	GRADIENT_MAX_HOP("gradient_max_hop"),
	GRADIENT_HOP("gradient_hop"),
	
	DIFFUSION_ONCE("diffusion_once"),
	
	
	
	DIFFUSION_OP("diffusion_op"), 
	
	AGGREGATION_OP ("aggregation_op"), 
	
	FIELD_VALUE ("field_value"),
	
	SOURCE ("source"), 

	PREVIOUS ("previous"), 
	
	DESTINATION ("destination"),
	
	ACTIVE_PROPAGATION("active_propagation"),
	
	// Used for the DECAY ECO-LAW, workes as a TIME-TO-LIVE
	DECAY ("decay"); 
	
	private PropertyName (final String text) {
        this.text = text;
    }

    private final String text;

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return text;
    }

	
	public static boolean isDecay(String s){
		return s.toLowerCase().equals(DECAY.toString());
	};
	
	private static final int decayDecrement = 1;
	
	public static boolean isDecayValue (String s) {
        
        try {

            Integer.parseInt(s);
        
        } catch (NumberFormatException ex) {
            return false;
        }
        
        return true;
    }
	
	public static Integer getDecayValue(String s){
		
		Integer n = null;
		
		try {

            n = Integer.parseInt(s);
        
        } catch (NumberFormatException ex) {
            
        }
		
		return n;
	}
	
	public static Integer decrement (Integer n){		
		return n - decayDecrement;
	}
	
	public static boolean isExpired(Integer n){
		return n.intValue() == 0;
	}
	
}
