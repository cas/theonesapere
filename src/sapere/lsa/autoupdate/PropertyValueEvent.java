package sapere.lsa.autoupdate;

import java.util.EventObject;

// The event
public class PropertyValueEvent extends EventObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5461269044381723264L;
	private String propertyName;
    private String value;
    
    public PropertyValueEvent( Object source, String propertyName, String value ) {
        super( source );
        this.propertyName = propertyName;
        this.value = value;
    }
    
    public String getPropertyName() {
        return propertyName;
    }
    
    public String getValue() {
        return value;
    }
    
}
