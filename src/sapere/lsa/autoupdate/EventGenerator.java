package sapere.lsa.autoupdate;

import java.util.Iterator;
import java.util.Vector;

// Event Generator
public abstract class EventGenerator {
	
	 private Vector<PropertyValueListener> listeners = new Vector<PropertyValueListener>();
	 
	 private String propertyName = null;
	 
	 private boolean onAppend = false;
	 
	 public EventGenerator(){
	 }
	 
	 public EventGenerator(boolean onAppend){
		 
		 this.onAppend = onAppend;
	 }
	 
	 public void setPropertyName(String propertyName){
		 this.propertyName = propertyName;
	 }
    
    public final void autoUpdate(String s) {

            firePropertyValueEvent(s);
        }

    public final synchronized void addPropertyValueListener( PropertyValueListener l ) {
        listeners.add( l );
    }
    
    public final synchronized void removePropertyValueListener( PropertyValueListener l ) {
        listeners.remove( l );
    }
    

     
    private final synchronized void firePropertyValueEvent(String s) {
    	
        PropertyValueEvent newEvent = new PropertyValueEvent( this, propertyName, s );
        
        Iterator<PropertyValueListener> pvListeners = listeners.iterator();
        while( pvListeners.hasNext() ) {
        	
        	if (onAppend = true)
        		( (PropertyValueListener) pvListeners.next() ).PropertyValueGenerated( newEvent );
        	else 
        		( (PropertyValueListener) pvListeners.next() ).PropertyValueAppendGenerated( newEvent );
        }
    }
}
