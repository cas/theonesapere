package sapere.lsa.interfaces;

import sapere.lsa.Id;



/**
 * 
 */



/**
 * @author Gabriella Castellimport eu.sapere.middleware.lsa.SubDescriptionId;
i (UNIMORE)
 *
 */
public interface ISubDescription {
	
	public Id getId();
	
	public String toString();
	
	// Add getPropertyValue(String name)
	
	// Add hasProperty(String name)
	
	// Add hasBond (String name)
	
	// Add getName ()

}
