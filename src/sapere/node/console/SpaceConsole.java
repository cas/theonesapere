package sapere.node.console;

import sapere.lsa.Lsa;

public interface SpaceConsole {
	public void startConsole(String nodeName);
	public void update(final Lsa[] list);
	
	public void updateGUI(Lsa[] list);
	public void setConsoleVisible();

}
