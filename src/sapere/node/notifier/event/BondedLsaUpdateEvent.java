/**
 * 
 */
package sapere.node.notifier.event;

import sapere.lsa.Lsa;
import sapere.lsa.interfaces.ILsa;


/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class BondedLsaUpdateEvent extends AbstractSapereEvent{
	
	
	private Lsa bondedLsa = null;

	public BondedLsaUpdateEvent(Lsa bondedLsa){
		
		this.bondedLsa = bondedLsa;
	}
	
	public ILsa getLsa(){
		return bondedLsa;
	}
	
	

}
