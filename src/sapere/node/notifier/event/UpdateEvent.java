/**
 * 
 */
package sapere.node.notifier.event;

import sapere.lsa.Lsa;


/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class UpdateEvent extends AbstractSapereEvent{
	
	private Lsa lsa = null;

	public UpdateEvent(Lsa lsa){
		this.lsa = lsa;
	}
	
	public Lsa getLsa(){
		return lsa;
	}

}
