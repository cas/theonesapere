package sapere.node.notifier.event;

import sapere.lsa.Lsa;
import sapere.lsa.interfaces.ILsa;

public class PropagationEvent extends AbstractSapereEvent{
	
	private Lsa lsa = null;
	
	public PropagationEvent(Lsa lsa){
		this.lsa = lsa;
	}
	
	public ILsa getLsa(){
		return lsa;
	}

}
