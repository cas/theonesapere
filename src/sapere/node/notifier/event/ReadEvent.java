/**
 * 
 */
package sapere.node.notifier.event;

import sapere.lsa.Lsa;


/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class ReadEvent extends AbstractSapereEvent{
	
	private Lsa lsa = null;

	public ReadEvent(Lsa lsa){
		this.lsa = lsa;
	}
	
	public Lsa getLsa(){
		return lsa;
	}

}
