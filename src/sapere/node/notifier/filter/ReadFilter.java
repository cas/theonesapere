/**
 * 
 */
package sapere.node.notifier.filter;

import sapere.lsa.Id;
import sapere.node.notifier.event.IEvent;
import sapere.node.notifier.event.ReadEvent;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */

public class ReadFilter implements IFilter{
	
	private Id targetLsaId = null;
	private String requestingId = null;
	
	public ReadFilter(Id lsaId, String requestingId){
		this.targetLsaId = lsaId;
		this.requestingId = requestingId;
	}

	
	public boolean apply(IEvent event) {
		
		boolean ret = false;
		
	    ReadEvent readEvent = (ReadEvent) event;
	    
	    if (readEvent.getLsa().getId().toString().equals(targetLsaId.toString()) && readEvent.getRequiringAgent().equals(requestingId))
	    	ret = true;
		
		return ret;
	}


	public boolean apply(IEvent event, String lsaSubscriberId) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean equals (Object o){
		boolean ret = false; 
		if (o instanceof ReadFilter){
		ret = ( targetLsaId.toString().equals( ((ReadFilter) o).targetLsaId.toString()) &&
				requestingId.equals(((ReadFilter) o).requestingId));
		}
		return ret;
	}

}
