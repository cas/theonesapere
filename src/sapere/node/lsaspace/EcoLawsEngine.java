/**
 * 
 */
package sapere.node.lsaspace;

import java.util.List;
import java.util.logging.Logger;
import com.google.common.collect.Lists;
import extension.SapereProperties;
import one.core.Settings;
import sapere.node.NodeManager;
import sapere.node.lsaspace.ecolaws.Aggregation;
import sapere.node.lsaspace.ecolaws.Bonding;
import sapere.node.lsaspace.ecolaws.Decay;
import sapere.node.lsaspace.ecolaws.GradientAggregation;
import sapere.node.lsaspace.ecolaws.IEcoLaw;
import sapere.node.lsaspace.ecolaws.Propagation;

/**
 * @author Gabriella Castelli (UNIMORE)
 * @author Graeme Stevenson (STA)
 */
public class EcoLawsEngine implements Runnable {

	/**
	 * The default frequency for firing eco-laws if not specified in the
	 * property file.
	 */
	private static final int DEFAULT_FIRING_FREQUENCY = 10;

	/**
	 * The default number of cycles between triggering the eco-laws.
	 */
	private long my_defaultFrequency = DEFAULT_FIRING_FREQUENCY;
	/**
	 * The local sim cycle counter
	 */
	private long my_cycleCounter = 0;

	public static final String ECO_LAWS_AGENT = "EcoLawsAgent";

	public static final String ECO_LAWS_FIRING = "EcoLawsEngine.FiringFrequency";

	public static final String ECO_LAWS_PROPAGATION = "EcoLawsEngine.DirectPropagationFrequency";

	public static final String ECO_LAWS_GRADIENT = "EcoLawsEngine.SpreadingFrequency";

	public static final String ECO_LAWS_GRADIENT_AGGREGATION = "EcoLawAggregation";

	public static final String ECO_LAWS_DECAY = "EcoLawsDecay";

	public static final String ECO_LAWS_AGGREGATION = "EcoLawsAggregation";

	private Settings my_settings;

	private static final Logger LOGGER = Logger.getLogger("sapere.node.lsaspace");

	private final List<IEcoLaw> my_ecoLaws = Lists.newLinkedList();
	private Space mySpace;

	public EcoLawsEngine(Space space, NodeManager a_nodeManager, Settings settings) {

		my_settings = settings;
		SapereProperties.ECOLAW_FREQ = loadEcoLawFrequency(ECO_LAWS_FIRING);
		SapereProperties.ECOLAW_PROP_FREQ = loadEcoLawFrequency(ECO_LAWS_PROPAGATION);
		SapereProperties.ECOLAW_GRADIENT_FREQ = loadEcoLawFrequency(ECO_LAWS_GRADIENT);

		// Add the eco-laws in execution order.
		my_ecoLaws.add(new GradientAggregation(a_nodeManager));
		my_ecoLaws.add(new Decay(a_nodeManager));
		my_ecoLaws.add(new Aggregation(a_nodeManager));
		my_ecoLaws.add(new Bonding(a_nodeManager));
		my_ecoLaws.add(new Propagation(a_nodeManager));
		mySpace = space;
	}

	/**
	 * Loads an eco-law frequency from the simulator property file.
	 * 
	 * @param property
	 *            the name of the property to load.
	 * @return the frequency specified in the property file, or the default
	 *         value if not found.
	 */
	public long loadEcoLawFrequency(String property) {
		if (my_settings.contains(property)) {
			return Long.parseLong(my_settings.getSetting(property));
		} else {
			LOGGER.warning("No frequency specified in the settings file (<App>." + property + "). Defaulting to "
					+ my_defaultFrequency + " cycle(s).");
			return my_defaultFrequency;
		}
	}

	public void run() {
		// Increment the execution cycle counter.
		my_cycleCounter++;
		LOGGER.finest("Starting EcoLawsManager cycle " + my_cycleCounter);

		if (SapereProperties.CYCLE_COUNTER % SapereProperties.ECOLAW_FREQ == 0) {
			this.mySpace.getSyncBuffer().syncWithExternal();
			for (IEcoLaw law : my_ecoLaws) {
				law.invoke();
			}
			mySpace.updateConsole();
		}

		LOGGER.finest("Stopping EcoLawsManager cycle " + my_cycleCounter);
	}

	/**
	 * Adds an eco-law to the end of the execution cycle.
	 * 
	 * @param an_ecoLaw
	 *            the eco-law to add to the execution cycle.
	 */
	public void addEcoLaw(final IEcoLaw an_ecoLaw) {
		my_ecoLaws.add(an_ecoLaw);
	}

	/**
	 * Inserts an eco-law to the execution cycle at the indicated position.
	 * 
	 * @param an_index
	 *            the position in the eco-law list to add the eco-law.
	 * @param an_ecoLaw
	 *            the eco-law to add to the execution cycle.
	 */
	public void addEcoLaw(final int an_index, final IEcoLaw an_ecoLaw) {
		my_ecoLaws.add(an_index, an_ecoLaw);
	}

	/**
	 * Replaces the eco-law set with a new set of eco-laws.
	 * 
	 * @param some_ecoLaws
	 *            the eco-laws to run at the execution cycle.
	 */

}
