/**
 * 
 */
package sapere.node.lsaspace.ecolaws.spreading;

import one.core.DTNHost;
import sapere.lsa.Lsa;
import sapere.node.NodeManager;

/**
 * A location-based spreading eco-law.
 * 
 * @author Graeme Stevenson (STA)
 */
public class RegularSpreading extends AbstractSpreadingEcoLaw {

	/**
	 * Creates a new instance of the location based spreading eco-law.
	 * 
	 * @param a_nodeManager
	 *            the node manager of the space.
	 * @param a_dtnHost
	 *            the hosting node.
	 * @param dtnHost
	 */
	public RegularSpreading(final NodeManager a_nodeManager,
			final DTNHost a_dtnHost) {
		super(a_nodeManager, a_dtnHost);
	}

	
	/**
	 * Whether this LSA should be spread.
	 */
	public boolean shouldLsaBeSpread(final Lsa lsa) {
		boolean r1 = isSpreadable(lsa);
		boolean r2 = hasNotAlreadySpread(lsa);
		return  r1 && r2;

	}

	/**
	 * Adds a spread_from property containing the coordinated of this space.
	 * 
	 * @param an_lsa
	 *            the LSA to tag.
	 */
	@Override
	public void beforeSpreading(Lsa an_lsa) {
		// No preprocessing required.
	}


}
