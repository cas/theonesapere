package sapere.node;

import extension.SapereProperties;
import one.core.DTNHost;
import one.core.Settings;
import sapere.node.console.SpaceConsole;
import sapere.node.console.SpaceConsoleImpl;
import sapere.node.lsaspace.EcoLawsEngine;
import sapere.node.lsaspace.OperationManager;
import sapere.node.lsaspace.Space;
import sapere.node.networking.INetworkDeliveryManager;
import sapere.node.networking.INetworkReceiverManager;
import sapere.node.networking.NetworkDeliveryManager;
import sapere.node.networking.NetworkReceiverManager;
import sapere.node.notifier.Notifier;
import sapere.node.util.SystemConfiguration;

/**
 * Initializes the Local Sapere Node
 * 
 * @author Gabriella Castelli (UNIMORE)
 * @author Francesco De Angelis (UNIGENE)
 */
public class NodeManager {

	/** The Operation Manager */
	private OperationManager operationManager = null;

	/** The eco-laws engine */
	private EcoLawsEngine ecoLawsEngine = null;

	/** Whether or note this node has GPS enabled */
	private final boolean gpsOn;

	/** The Network Delivery Manager */
	private INetworkDeliveryManager networkDeliveryManager = null;

	/** The Network Receiver Mananger */
	private INetworkReceiverManager networkReceiverManager = null;

	/** The local LSA space */
	private Space space = null;

	/** The local Notifier */
	private Notifier notifier = null;

	/** The name of the local Node */
	private static DTNHost my_host = null;

	/** The Console of the LSA space */
	private SpaceConsole console = null;

	/**
	 * The Settings file used to control simulation parameters.
	 */
	private final Settings my_settings;

	/**
	 * Returns the name of this Node
	 * 
	 * @return The Name of this Node
	 */
	public String getSpaceName() {
		return my_host.getName();
	}
	
	public DTNHost getHost()
	{
		return this.my_host;
	}

	/**
	 * Returns the space associated with this host.
	 * 
	 * @return The Name of this Node
	 */
	public Space getSpace() {
		return space;
	}

	// /**
	// * This is the only way to access the singleton instance.
	// * @return A reference to the singleton.
	// */
	// // Provides well-known access point to singleton EventService
	// public NodeManager instance() {
	//
	// }

	/**
	 * Starts the Node Manager
	 */
	// Prevents direct instantiation of the event service
	public NodeManager(DTNHost a_host, Settings the_settings) {
		SystemConfiguration.getInstance();

		// Sets the GPS.
		gpsOn = Math.random() < SapereProperties.GPS_RATIO ? true : false;

		my_host = a_host;
		my_settings = the_settings;

		// this.notifier = Notifier.instance();
		this.notifier = new Notifier();

		if (!SapereProperties.BATCH_MODE) {
			this.console = new SpaceConsoleImpl();
			console.startConsole(a_host.getName());
		}

		space = new Space(a_host.getName(), notifier, console);

		if (SystemConfiguration.getInstance().props != null)
			operationManager = new OperationManager(space, notifier,
					a_host.getName(), new Long(
							SystemConfiguration.getInstance().props
									.getProperty("opTime")).longValue(),
					new Long(SystemConfiguration.getInstance().props
							.getProperty("sleepTime")).longValue());
		else
			operationManager = new OperationManager(space, notifier,
					a_host.getName());

		this.networkDeliveryManager = new NetworkDeliveryManager(a_host);
		this.networkReceiverManager = new NetworkReceiverManager(a_host,
				operationManager,space);
		
		space.getSyncBuffer().setNetworkReceiverInterface(networkReceiverManager,
				networkDeliveryManager);
		
		
		

		ecoLawsEngine = new EcoLawsEngine(space, this, my_settings);

		operationManager.setEcoLawsEngine(ecoLawsEngine);

		// Commented out to make single threaded version
		// new Thread(operationManager).start();
		// new Thread(ecoLawsEngine).start();

		// new NetworkTopologyManager(this);

		// RemoteConnectionManager remoteMng = new RemoteConnectionManager();
	}

	/**
	 * Retrieves the local Operation Manager
	 * 
	 * @return the local Operation Manager
	 */
	public OperationManager getOperationManager() {
		return operationManager;
	}

	/**
	 * Retrieves the local Eco Law Engine
	 * 
	 * @return the local Eco Law engine
	 */
	public EcoLawsEngine getEcoLawsEngine() {
		return ecoLawsEngine;
	}

	/**
	 * Retireves the local Notifier
	 * 
	 * @return the local Notifier
	 */
	public Notifier getNotifier() {
		return notifier;
	}

	public void setConsoleVisible() {
		this.console.setConsoleVisible();
	}

	/**
	 * Retrieves a reference to the network delivery manager.
	 */
	public INetworkDeliveryManager getNetworkDeliveryManager() {
		return networkDeliveryManager;
	}

	/**
	 * Retrieves a reference to the network receiver manager.
	 */
	public INetworkReceiverManager getNetworkReceiverManager() {
		return networkReceiverManager;
	}

	/**
	 * Is GPS enabled on this node?
	 * @return
	 */
	public boolean isGPSEnabled() {
		return gpsOn;
	}

}
