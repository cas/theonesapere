package one.smartEnergy;

public class Payload {

	private String price;
	private String quantity;
	private String availability;
	private String type;

	public Payload(String price, String availability, String quantity, String type) {
		this.price = price;
		this.quantity = quantity;
		this.availability = availability;
		this.type = type;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

}
