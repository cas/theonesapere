package one.smartEnergy;

/*
 * This is an agent creates a custom property in its
 * LSA.
 */
import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

public class AgentProperties extends SapereAgent {

	private final String agentName;
	private final Payload payload;

	/**
	 * Creates a new agent.
	 * 
	 * @param name
	 *            the name of the node
	 * @param an_opMng
	 *            the operation manager for the node.
	 */
	public AgentProperties(String name, Payload payload, NodeManager a_nodeManager) {
		super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
		this.agentName = name;
		this.payload = payload;
	}

	/**
	 * {@inheritDoc}
	 */
	public void onBondAddedNotification(BondAddedEvent event) {
	}

	/**
	 * {@inheritDoc}
	 */
	public void onBondRemovedNotification(BondRemovedEvent event) {
	}

	/**
	 * {@inheritDoc}
	 */
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
	}

	/**
	 * {@inheritDoc}
	 */
	public void onPropagationEvent(PropagationEvent event) {
	}

	/**
	 * {@inheritDoc}
	 */
	public void setInitialLSA() {
		//System.out.println("Created agent " + agentName);

		// we create a new custom property in the LSA
		addProperty(new Property("Price", payload.getPrice()));
		addProperty(new Property("Availability", payload.getAvailability()));
		addProperty(new Property("Quantity", payload.getQuantity()));
		addProperty(new Property("Type", payload.getType()));

		// NOTE: do not forget to invoke this method to
		// update the LSA of the agent
		submitOperation();
	}

	public void onReadNotification(ReadEvent readEvent) {
	}

	public void onDecayedNotification(DecayedEvent event) {
	}
}