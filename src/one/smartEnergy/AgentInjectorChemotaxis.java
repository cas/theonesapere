package one.smartEnergy;

import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;


public class AgentInjectorChemotaxis extends SapereAgent {
	private String chemoDestination;
	private String agentName;
	public AgentInjectorChemotaxis(String name, String chemoDestination, NodeManager a_nodeManager) {
		super("MyAgent", a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
		this.chemoDestination = chemoDestination;
		agentName = name;

	}

	@Override
	public void setInitialLSA() {
		//this.removeSubDescription("chemo1");
		//this.removeSubDescription("chemo");
		//this.removeSubDescription("chemo2");
		this.addProperty(new Property("chemoDestination", chemoDestination));	
		this.addProperty(new Property("sourceChemo", agentName));
		this.addDirectPropagationOnce(chemoDestination);
		this.submitOperation();
		System.out.println("+++ AgentInjectorChemotaxis created +++ "+ agentName);


	}

	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
	}

	@Override
	public void onDecayedNotification(DecayedEvent event) {
	}

	@Override
	public void onReadNotification(ReadEvent readEvent) {
	}

}
