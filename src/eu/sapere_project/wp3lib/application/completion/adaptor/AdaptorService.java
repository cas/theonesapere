package eu.sapere_project.wp3lib.application.completion.adaptor;

import java.util.Vector;

import sapere.agent.ImplicitRRAgent;
import sapere.agent.implicitirr.IRRAsynchronousService;
import sapere.lsa.Lsa;
import sapere.node.NodeManager;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

public class AdaptorService extends ImplicitRRAgent implements IRRAsynchronousService {

	AdaptorImplementation i = null;

	public AdaptorService(String name, NodeManager a_nodeManager) {
		super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
	}

	public void setInitialLSA(AdaptorImplementation i) {
		this.i = i;

		// gps-traces-completion = !
		// gps-traces = ?
		this.addProperty("gps-traces-completion", "gps-traces", this);
	}

	@Override
	public void setInitialLSA() {

	}

	@Override
	public void onBondedLsaNotification(Lsa bondedLsa) {

		Vector<String> res = bondedLsa.getProperty("gps-traces").getPropertyValue().getValue();

		// Run the implementation of this service: gets the gps traces to be
		// processed and pass them
		// to the AdaptorImplementation SapereAgent
		i.setInitialLSA(bondedLsa.getProperty("gps-traces").getPropertyValue().getValue());
	}

	// Called by the AdaptorImplementation SapereAgent
	public void setResponse(Vector<String> pv) {
		this.setPotentialPropertyValue(pv);
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		// TODO Auto-generated method stub

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onReadNotification(ReadEvent readEvent) {
		// TODO Auto-generated method stub

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onDecayedNotification(DecayedEvent event) {
		// TODO Auto-generated method stub

	}

}
