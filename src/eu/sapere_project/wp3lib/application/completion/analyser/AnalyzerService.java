package eu.sapere_project.wp3lib.application.completion.analyser;

import java.util.Vector;

import sapere.agent.ImplicitRRAgent;
import sapere.agent.implicitirr.IRRAsynchronousService;
import sapere.lsa.Lsa;
import sapere.node.NodeManager;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

public class AnalyzerService extends ImplicitRRAgent implements IRRAsynchronousService {

	AnalyzerImplementation i = null;

	public AnalyzerService(String name, NodeManager a_nodeManager) {
		super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
		// TODO Auto-generated constructor stub
	}

	public void setInitialLSA(AnalyzerImplementation i) {
		this.i = i;
		this.addProperty("gps-traces-analyzer", "gps-traces", this);
	}

	@Override
	public void onBondedLsaNotification(Lsa bondedLsa) {
		i.setInitialLSA(bondedLsa.getProperty("gps-traces").getPropertyValue().getValue());
	}

	public void setResponse(Vector<String> pv) {
		this.setPotentialPropertyValue(pv);
	}

	@Override
	public void setInitialLSA() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		// TODO Auto-generated method stub

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onReadNotification(ReadEvent readEvent) {
		// TODO Auto-generated method stub

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onDecayedNotification(DecayedEvent event) {
		// TODO Auto-generated method stub

	}

}
