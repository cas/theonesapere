// Taken from the SAPERE WP3 Libraries
// (c) Copyright 2011, 2012 Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package eu.sapere_project.wp3lib.extension.templates;

import sapere.lsa.Property;
import sapere.lsa.interfaces.ILsa;

/**
 * Applies chemotaxis to an LSA.
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 */
public final class RequestInvoker implements Pattern {

   /**
    * The keyword for the request.
    */
   private final String my_requestKeyword;

   /**
    * Creates an request looking for a source that understands the supplied keyword.
    * @param a_requestKeyword the request keyword.
    */
   public RequestInvoker(final String a_requestKeyword) {
      my_requestKeyword = a_requestKeyword;
   }

   /**
    * Utility method to apply to an LSA and return the template to support assignment.
    * @param a_requestKeyword the request keyword.
    * @param an_lsa the LSA to apply the transform to.
    * @return the invoker, after application to the LSA.
    */
   public static RequestInvoker createAndApply(final String a_requestKeyword, final ILsa an_lsa) {
      final RequestInvoker result = new RequestInvoker(a_requestKeyword);
      result.applyToLSA(an_lsa);
      return result;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void applyToLSA(final ILsa an_lsa) {
      if (an_lsa.getSingleValue("request_keyword").isPresent()) {
         an_lsa.getProperty("request_keyword").getValue().add(my_requestKeyword);
      } else {
         an_lsa.addProperty(new Property("request_keyword", my_requestKeyword));
      }
      an_lsa.addSubDescription("resp:" + my_requestKeyword, new Property("complete_classification", "*"), new Property(
            "chemotaxisResponseId", my_requestKeyword));
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void removeFromLSA(final ILsa an_lsa) {
      if (an_lsa.getSingleValue("request_keyword").isPresent()) {
         an_lsa.getProperty("request_keyword").getValue().remove(my_requestKeyword);
      } else {
         an_lsa.removeProperty("request_keyword");
      }
      an_lsa.removeSubDescription("resp:" + my_requestKeyword);
   }

}
