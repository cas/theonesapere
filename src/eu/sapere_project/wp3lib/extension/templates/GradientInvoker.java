// Taken from the SAPERE WP3 Libraries
// (c) Copyright 2011, 2012 Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package eu.sapere_project.wp3lib.extension.templates;

import sapere.lsa.Property;
import sapere.lsa.interfaces.ILsa;
import sapere.lsa.values.PropertyName;
import eu.sapere_project.wp3lib.extension.ecolaw.AggregationPredicate.OP;

/**
 * Applies gradient to an LSA.
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 */
public final class GradientInvoker implements Pattern {

   /**
    * The source name for the gradient.
    */
   private final String my_sourceName;

   /**
    * The gradient value field.
    */
   private final String my_gradientField;

   /**
    * The gradient hop count.
    */
   private final String my_hopCount;

   /**
    * Creates a gradient template for a given source name.
    * @param a_sourceName the source name for this gradient
    * @param a_gradientField the field on which the gradient comparison is based.
    * @param a_hopCount the maximum hop count of the gradient.
    */
   public GradientInvoker(final String a_sourceName, final String a_gradientField, final int a_hopCount) {
      my_sourceName = a_sourceName;
      my_gradientField = a_gradientField;
      my_hopCount = Integer.valueOf(a_hopCount).toString();
   }

   /**
    * {@inheritDoc}
    */
   public void applyToLSA(final ILsa an_lsa) {
      an_lsa.addProperty(new Property(PropertyName.SOURCE.toString(), my_sourceName));
      an_lsa.addProperty(new Property(PropertyName.DIFFUSION_OP.toString(), "GRADIENT_" + my_hopCount));
      an_lsa.addProperty(new Property(PropertyName.PREVIOUS.toString(), "local"));
      an_lsa.addProperty(new Property(PropertyName.DESTINATION.toString(), "default"));
      new AggregationInvoker("source", my_sourceName, my_gradientField, OP.MAX).applyToLSA(an_lsa);
   }

   /**
    * Utility method to apply to an LSA and return the template to support assignment.
    * @param a_sourceName the source name for this gradient
    * @param a_gradientField the field on which the gradient comparison is based.
    * @param a_hopCount the maximum hop count of the gradient.
    * @param an_lsa the LSA to apply the transform to.
    * @return the invoker, after application to the LSA.
    */
   public static GradientInvoker createAndApply(final String a_sourceName, final String a_gradientField,
         final int a_hopCount, final ILsa an_lsa) {
      final GradientInvoker result = new GradientInvoker(a_sourceName, a_gradientField, a_hopCount);
      result.applyToLSA(an_lsa);
      return result;
   }

   /**
    * {@inheritDoc}
    */
   public void removeFromLSA(final ILsa an_lsa) {
      an_lsa.removeProperty(PropertyName.DIFFUSION_OP.toString());
      an_lsa.removeProperty(PropertyName.AGGREGATION_OP.toString());
      an_lsa.removeProperty(PropertyName.PREVIOUS.toString());
      an_lsa.removeProperty(PropertyName.DESTINATION.toString());
      new AggregationInvoker("source", my_sourceName, my_gradientField, OP.MAX).removeFromLSA(an_lsa);
   }

}
