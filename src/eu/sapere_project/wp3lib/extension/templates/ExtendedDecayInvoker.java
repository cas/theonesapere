// Taken from the SAPERE WP3 Libraries
// (c) Copyright 2011, 2012 Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package eu.sapere_project.wp3lib.extension.templates;

import sapere.lsa.Property;
import sapere.lsa.interfaces.ILsa;

/**
 * Applies decay to an LSA.
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 */
public class ExtendedDecayInvoker implements Pattern {

   /**
    * A decay pattern for 100 cycles.
    */
   public static final ExtendedDecayInvoker EXP_DECAY_100 = new ExtendedDecayInvoker("expDecayValue", 100, -0.002d);

   /**
    * The decay value.
    */
   private final String my_decayValue;

   private final String my_decayField;

   private final String my_decayMultiplier;

   /**
    * Creates a decay template for a given integer value.
    * @param a_decayValue the initial decay value for this template.
    */
   public ExtendedDecayInvoker(final String a_decayField, final int a_decayValue, final double a_decayMultiplier) {
      my_decayField = a_decayField;
      my_decayValue = Double.toString(a_decayValue);
      my_decayMultiplier = Double.toString(a_decayMultiplier);
   }

   /**
    * {@inheritDoc}
    */
   public void applyToLSA(final ILsa an_lsa) {
      an_lsa.addProperty(new Property("exp_decay_field", my_decayField));
      an_lsa.addProperty(new Property(my_decayField, my_decayValue));
      an_lsa.addProperty(new Property("exp_decay_multiplier", my_decayMultiplier));
   }

   /**
    * {@inheritDoc}
    */
   public void removeFromLSA(final ILsa an_lsa) {
      an_lsa.removeProperty("exp_decay_field");
      an_lsa.removeProperty("exp_decay_multiplier");
      an_lsa.removeProperty(my_decayField);
   }

}
