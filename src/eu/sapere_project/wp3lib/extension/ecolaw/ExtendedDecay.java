/**
 * 
 */
package eu.sapere_project.wp3lib.extension.ecolaw;

import sapere.lsa.Lsa;
import sapere.lsa.Property;
import sapere.node.NodeManager;
import sapere.node.lsaspace.EcoLawsEngine;
import sapere.node.lsaspace.ecolaws.AbstractEcoLaw;

/**
 * @author Graeme Stevenson
 */
public class ExtendedDecay extends AbstractEcoLaw {

   /**
    * Creates a new instance of the chemotaxis eco-law.
    * @param a_space the space in which the law will execute.
    * @param a_nodeManager the node manager of the space.
    */
   public ExtendedDecay(final NodeManager a_nodeManager) {
      super(a_nodeManager);
   }

   /**
    * Invokes the decay eco-law by selecting relevant LSAs and applying the decay transformation.
    */
   public void invoke() {
      for (Lsa lsa : getLSAs()) {
         if (lsa.getSingleValue("exp_decay_field").isPresent()
               && lsa.getSingleValue("exp_decay_multiplier").isPresent()) {
            applyToLsa(lsa);
         }
      }
   }

   /**
    * Applies the decay transformation to an LSA.
    * @param an_lsa the LSA to decay.
    */
   public void applyToLsa(final Lsa an_lsa) {
      // decrement the TTL
      String field = an_lsa.getSingleValue("exp_decay_field").get();
      Double decay = Double.parseDouble(an_lsa.getSingleValue(field).get());
      Double multiplier = Double.parseDouble(an_lsa.getSingleValue("exp_decay_multiplier").get());
      Integer decayAppliedCount = 0;
      try {
         decayAppliedCount = Integer.parseInt(an_lsa.getSingleValue("exp_decay_count").get());
      } catch (Exception e) {
         // No count, therefore make 0.
      }
      decayAppliedCount++;
      if (decay != null) {
         // decrement the TTL - add age
         decay = decay * Math.exp(multiplier * decayAppliedCount);

         if (decay.intValue() <= 0) {
            remove(an_lsa);
         } else {
            Lsa copy = an_lsa;
            copy.addProperty(new Property(field, decay.toString()));
            copy.addProperty(new Property("exp_decay_count", decayAppliedCount.toString()));
            update(copy);

         }
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean doesSpread() {
      return false;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean doesDirectPropagation() {
      return false;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getName() {
      return EcoLawsEngine.ECO_LAWS_DECAY;
   }
}
