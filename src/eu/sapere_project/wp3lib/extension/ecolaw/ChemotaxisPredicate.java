// [Project Title]
// (c) Copyright 2011, 2012 Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package eu.sapere_project.wp3lib.extension.ecolaw;

import sapere.lsa.Lsa;

import com.google.common.base.Predicate;

/**
 * A predicate for examining if an LSA should have the Chemotaxis eco-law applied to it.
 * @author Graeme Stevenson (graeme.stevenson@st-andrews.ac.uk)
 */
public class ChemotaxisPredicate implements Predicate<Lsa> {

   /**
    * Checks an Lsa for the necessary Chemotaxis properties.
    */
   @Override
   public boolean apply(final Lsa an_lsa) {
      return an_lsa.getSingleValue("chemotaxisDestination").isPresent()
            && an_lsa.getSingleValue("chemotaxisMode").isPresent();
   }

}
