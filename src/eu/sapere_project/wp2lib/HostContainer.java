package eu.sapere_project.wp2lib;

import java.util.ArrayList;

import extension.SapereProperties.ChemoPath;

import one.core.DTNHost;

public class HostContainer{
	public static ArrayList<DTNHost> hosts = new ArrayList<DTNHost>();
	public static ArrayList<DTNHost> hostPath1 = null;
	public static ArrayList<DTNHost> hostPath2 = null;
	public static void addHost(DTNHost host)
	{
		hosts.add(host);
	}
	
	public static void resetChemo()
	{
		for (DTNHost host: hosts)
		{
			host.hasChemo = false;
			host.hasChemoPath = false;
		}
	}
	
	public static void resetChemoPath()
	{
		for (DTNHost host: hosts)
		{
			host.hasChemoPath = false;
		}
	}
	
	public static void setPathHost(ArrayList <DTNHost > hosts,
									ChemoPath chemo)
	{
		if (chemo == ChemoPath.CHEMO_1)
			hostPath1 = hosts;
		else if (chemo == ChemoPath.CHEMO_2) 
			hostPath2 = hosts;
	}
}
