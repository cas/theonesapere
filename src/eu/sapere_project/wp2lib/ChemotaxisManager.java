
package eu.sapere_project.wp2lib;

import extension.SapereProperties;
import one.core.DTNHost;
import sapere.agent.SapereAgent;
import sapere.lsa.Property;
import sapere.lsa.interfaces.ILsa;
import sapere.node.NodeManager;
import sapere.node.notifier.event.BondAddedEvent;
import sapere.node.notifier.event.BondRemovedEvent;
import sapere.node.notifier.event.BondedLsaUpdateEvent;
import sapere.node.notifier.event.DecayedEvent;
import sapere.node.notifier.event.PropagationEvent;
import sapere.node.notifier.event.ReadEvent;

/**
 * This class gather information about the gradient in order to use it for
 * evaluating the accuracy of the gradient. 
 * @author Jose Luis Fernandez-Marquez
 */
public class ChemotaxisManager extends SapereAgent {

   private final String my_nodeName;

   private final String my_value;
   
   private DTNHost host;

   /**
    * Creates a new agent.
    * @param name the name of the node
    * @param an_opMng the operation manager for the node.
    */
   public ChemotaxisManager(String name, NodeManager a_nodeManager, String a_value, DTNHost host) {
      super(name, a_nodeManager.getOperationManager(), a_nodeManager.getNotifier());
      my_nodeName = name;
      my_value = a_value;
      this.host = host;
   }

   /**
    * {@inheritDoc}
    */
   public void onBondAddedNotification(BondAddedEvent event) {
      // TODO Auto-generated method stub
	   //System.out.println("Node " + my_nodeName + " received the value");
	   if(SapereProperties.GRADIENTS_EVALUATION){
		   
		   if (event.getBondedLsa().getProperty("hopsTravelled") != null)
		   {
			   //System.out.println("GradientValue: " + event.getBondedLsa().
				//	   getProperty("hopsTravelled").getValue().elementAt(0).toString());
//			   this.host.setGradientValue(Integer.parseInt(event.getBondedLsa().
//					   getProperty("hopsTravelled").getValue().elementAt(0).toString()));
			   String gradientId = event.getBondedLsa().getProperty("name").
					   getValue().elementAt(0);
			   String previous = event.getBondedLsa().getProperty("previous").
					   getValue().elementAt(0);
			   
			   this.host.setPreviousNodeName(gradientId,previous);
			   //MapGraphic.c.addConnection(gradientId);
			   this.host.hasGradient = true;
			   this.host.hasChemo = false;
		   }
		   
	   }else{
		   this.host.setFlag(true);   
	   }
   }

   /**
    * {@inheritDoc}
    */
   public void onBondRemovedNotification(BondRemovedEvent event) {
      // TODO Auto-generated method stub
	   host.hasChemo = false;
	   if (this.host.getName().equals("Node25"))
	   if(SapereProperties.GRADIENTS_EVALUATION){
		   ILsa lsa = event.getLsa();
		   if (lsa.getProperty("gradient") != null)
		   {
			   host.hasChemo = false;
		   }
	}
   }

   /**
    * {@inheritDoc}
    */
   public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
      // TODO Auto-generated method stub
	   //host.hasChemo = false;
   }

   /**
    * {@inheritDoc}
    */
   public void onPropagationEvent(PropagationEvent event) {
   }

   /**
    * {@inheritDoc}
    */
   public void setInitialLSA() {
      //lsa.addProperty(new Property("NAME", my_nodeName));
      //lsa.addProperty(new Property("Example", "bonding-example"));
      //lsa.addProperty(new Property("a-bonding-keyword", my_value));
	   
	   if(SapereProperties.GRADIENTS_EVALUATION){
		   lsa.addSubDescription("q", new Property("hopsTravelled", "*"));
		   //lsa.addSubDescription("d", new Property("chemotaxis", "*"));
	   }else{
		   lsa.addSubDescription("q", new Property("data-value", "*")); 
	   }
	   submitOperation();
   }

public void onReadNotification(ReadEvent readEvent) {
	// TODO Auto-generated method stub
	
}

@Override
public void onDecayedNotification(DecayedEvent event) {
	// TODO Auto-generated method stub
	
}
}
