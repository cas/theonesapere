/*
 * The encrypted message sent across the network.
 * 
 * GPL v.3
 * 
 * Francesco De Angelis
 * 
 * francesco.deangelis@unige.ch
 * 
 * 
 */

package saperedemo.securitymanager;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.commons.codec.binary.Base64;

public class EncryptedMessage 
{
	public byte [] body;
	public byte [] iv;
	public byte [] sessionKey;
	public String serError;
	
	public EncryptedMessage()
	{
	}
	
	public EncryptedMessage(byte[] body, byte [] iv, byte [] sessionKey)
	{
		this.body = body;
		this.iv = iv;
		this.sessionKey = sessionKey;
	}
	
	public String encoded()
	{
        byte [][] multi = new byte[3][];
        multi[0] = body;
        multi[1] = iv;
        multi[2] = sessionKey;
        
		// new byte stream
		ByteArrayOutputStream bs = new ByteArrayOutputStream();
        // serialize the content of the message
        try
        {
    			// serializer
            ObjectOutputStream sr = new ObjectOutputStream(bs);
	        sr.writeObject(multi);
	        sr.close();
	        return Base64.encodeBase64String(bs.toByteArray());	
        }
        catch(Exception ex)
        {
        		return ex.getMessage();
        }
	}
	
	public static EncryptedMessage decode(String encoded)
	{
		EncryptedMessage mex = null;
		// decode the EncryptedMessage
		//
		try
		{
			ByteArrayInputStream bs = new ByteArrayInputStream(Base64.decodeBase64(encoded)); 
			ObjectInputStream iis = new ObjectInputStream(bs);
			byte [][] content = (byte[][]) iis.readObject();
			mex = new EncryptedMessage(content[0], content[1],content[2]);
			return mex;
		}
		catch(Exception ex)
		{
			mex = new EncryptedMessage();
			mex.serError = ex.getMessage();
			return mex;
		}
	}
}
