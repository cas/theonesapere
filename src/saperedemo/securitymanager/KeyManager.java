/*
 * A class implementing our security functionalities.
 * 
 * GPL v.3
 * 
 * Francesco De Angelis
 * 
 * francesco.deangelis@unige.ch
 * 
 * 
 */
package saperedemo.securitymanager;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class KeyManager 
{
	private KeyPair keyPair;
	private final int PB_KEY_SIZE = 2048;
	public final static String PB_ALG = "RSA";
	public final static String SY_ALG = "AES";
	public final static String PB_ALG_SP = PB_ALG + "/ECB/PKCS1Padding";
	public final static String SY_SP = SY_ALG + "/CBC/PKCS5Padding";
	
	public KeyManager() throws Exception
	{
		// generate a public key
		keyPair = generatePublicKey();
	}

	public void regeneratePKey() throws Exception
	{
		// generate a new public key
		keyPair = generatePublicKey();	
	}
	
	public PublicKey getGeneratedPublicKey()
	{
		return keyPair.getPublic();
	}
	
	private KeyPair generatePublicKey() throws Exception
	{
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(PB_ALG);
		keyPairGenerator.initialize(PB_KEY_SIZE);
		
	    return keyPairGenerator.genKeyPair();
	}
	
	public EncryptedMessage encMessage(String mex) throws Exception
	{	
		// encrypt the message by using a symmetric cipher
		EncryptedMessage encMex = encryptSymmetric(mex.getBytes());
		// encrypt the session key with the public key
		encryptSessionKey(encMex, keyPair.getPublic());
		return encMex;
	}
	
	public EncryptedMessage encMessage(String mex, PublicKey key) throws Exception
	{
		// encrypt the message by using a symmetric cipher
		EncryptedMessage encMex = encryptSymmetric(mex.getBytes());
		// encrypt the session key with the given public key
		encryptSessionKey(encMex, key);
		return encMex;
	}
	
	private EncryptedMessage encryptSymmetric(byte[] input) throws Exception
	{
		EncryptedMessage enc = new EncryptedMessage();
		// create a session key
		SecureRandom sr = new SecureRandom();
		// AES 128
		enc.sessionKey = new byte[16];
		enc.iv = new byte[16];
		sr.nextBytes(enc.sessionKey);
		sr.nextBytes(enc.iv);
		// create an AES cipher
		Cipher cipher = Cipher.getInstance(SY_SP);
		cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(enc.sessionKey,SY_ALG), 
				new IvParameterSpec(enc.iv));
		enc.body = cipher.doFinal(input);
		return enc;
	}
	
	private void encryptSessionKey(EncryptedMessage encMex, PublicKey key) throws Exception
	{
		byte [] result;
		Cipher cipher = Cipher.getInstance(PB_ALG_SP);
		
		// Initialize PBE Cipher with key and parameters
		cipher.init(Cipher.ENCRYPT_MODE, key);
		// Encrypt the encoded Private Key with the PBE key
		encMex.sessionKey = cipher.doFinal(encMex.sessionKey);
		encMex.iv = cipher.doFinal(encMex.iv);
	}
	
	public EncryptedMessage decMessage(EncryptedMessage encMex) throws Exception
	{	
		// decrypt the session key and the initializator vector
		decryptSessionKey(encMex, keyPair);
		// decrypt the message
		decryptSymmetric(encMex);
		return encMex;
	}
	
	private void decryptSessionKey(EncryptedMessage encMex, KeyPair rsaKey) throws Exception
	{
		byte [] result;
		Cipher cipher = Cipher.getInstance(PB_ALG_SP);
		// Initialize cipher
		cipher.init(Cipher.DECRYPT_MODE, rsaKey.getPrivate());
		// Decrypt the encoded Private Key with the PBE key
		encMex.sessionKey = cipher.doFinal(encMex.sessionKey);
		encMex.iv = cipher.doFinal(encMex.iv);
	}
	
	private void decryptSymmetric(EncryptedMessage encMex) throws Exception
	{
		Cipher cipher = Cipher.getInstance(SY_SP);
		cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(encMex.sessionKey,SY_ALG), 
				new IvParameterSpec(encMex.iv));
		encMex.body = cipher.doFinal(encMex.body);
	}
}
