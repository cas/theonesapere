#
# This is the settings file to run the simulation
# with a map created with OpenJump
#

#

# This is the name of our scenario
Scenario.name = Geneva-map-scenario

# we want to simulate connections
Scenario.simulateConnections = true

# NOTE: for the parameters we are considering the simulated time (in seconds)
# incremental step size
Scenario.updateInterval = 0.1

Scenario.endTime = 6000

## Interface-specific settings for each group
# type : which interface class the interface belongs to
# For different types, the sub-parameters are interface-specific
# For SimpleBroadcastInterface, the parameters are:
# transmitSpeed : transmit speed of the interface (bytes per second) 
# transmitRange : range of the interface (meters)

# "Bluetooth" interface for all nodes
Group1Interface.type = SimpleBroadcastInterface

# Speed in bytes per second
# Transmit speed of 2 Mbps = 250kBps
Group1Interface.transmitSpeed = 250k

# transmission range (in meters)
Group1Interface.transmitRange = 170

# Define only 1 group
Scenario.nrofHostGroups = 1

## Group-specific settings:
# groupID : Group's identifier. Used as the prefix of host names
# nrofHosts: number of hosts in the group
# movementModel: movement model of the hosts (valid class name from movement package)
# waitTime: minimum and maximum wait times (seconds) after reaching destination
# speed: minimum and maximum speeds (m/s) when moving on a path
# bufferSize: size of the message buffer (bytes)
# router: router used to route messages (valid class name from routing package)
# activeTimes: Time intervals when the nodes in the group are active (start1, end1, start2, end2, ...)
# msgTtl : TTL (minutes) of the messages created by this host group, default=infinite

## Group and movement model specific settings
# pois: Points Of Interest indexes and probabilities (poiIndex1, poiProb1, poiIndex2, poiProb2, ... )
#       for ShortestPathMapBasedMovement
# okMaps : which map nodes are OK for the group (map file indexes), default=all 
#          for all MapBasedMovent models
# routeFile: route's file path - for MapRouteMovement
# routeType: route's type - for MapRouteMovement


# group's id.
Group1.groupID = Group1

# 20 nodes in this group
Group1.nrofHosts = 60

# nodes move randomly 
Group1.movementModel = ShortestPathMapBasedMovement
#Group1.movementModel = RandomWaypoint


Group1.router = EpidemicRouter
Group1.routeFile = map/mapGeneva.wkt
#Group1.routeFile = map/multiRoutes1.wkt

# max buffer size
Group1.bufferSize = 15M

# minimum and maximum waiting time before moving again
# from one point to another one
Group1.waitTime = 0, 120

# only one interface 
Group1.nrofInterfaces = 1
Group1.interface1 = Group1Interface


# minimum and maximum speed when moving
# from one point to another one
Group1.speed = 1.5, 4.5

# Message TTL in minutes
Group1.msgTtl = 30

# group1 (pedestrians) specific settings
Group1.groupID = p


## Message creation parameters 
# How many event generators
Events.nrof = 1

# Class of the first event generator
Events1.class = MessageEventGenerator

# (following settings are specific for the MessageEventGenerator class)
# Creation interval in seconds (one new message every 25 to 35 seconds)
Events1.interval = 225,235

# Message sizes (500kB - 1MB)
Events1.size = 500k,1M

# every time we send a message, one host's address 
# is chosen in this range
Events1.hosts = 0,59

# Message ID prefix
Events1.prefix = E1


## Movement model settings
# seed for movement models' pseudo random number generator (default = 0)
MovementModel.rngSeed = 1

# World's size for Movement Models (in meters)
MovementModel.worldSize = 10000, 10000

# How long time to move hosts in the world before real simulation
MovementModel.warmup = 1000

## Map based movement -movement model specific settings
MapBasedMovement.nrofMapFiles = 1

MapBasedMovement.mapFile1 = map/mapGeneva.wkt
#MapBasedMovement.mapFile2 = data/main_roads.wkt
#MapBasedMovement.mapFile3 = data/pedestrian_paths.wkt
#MapBasedMovement.mapFile4 = data/shops.wkt

## Reports - all report names have to be valid report classes

# how many reports to load
Report.nrofReports = 1
# length of the warm up period (simulated seconds)
Report.warmup = 0
# default directory of reports (can be overridden per Report with output setting)
Report.reportDir = reports/Random-nodes-scenario
# Report classes to load
Report.report1 = MessageStatsReport

## Default settings for some routers settings
ProphetRouter.secondsInTimeUnit = 30
SprayAndWaitRouter.nrofCopies = 6
SprayAndWaitRouter.binaryMode = true

## Optimization settings -- these affect the speed of the simulation
## see World class for details.
Optimization.cellSizeMult = 5
#how we call the update method of a node
Optimization.randomizeUpdateOrder = true


## GUI settings

# GUI underlay image settings
GUI.UnderlayImage.fileName = data/mapGeneva.png
# Image offset in pixels (x, y)
GUI.UnderlayImage.offset = 0, 0
# Scaling factor for the image
GUI.UnderlayImage.scale = 1
# Image rotation (radians)
GUI.UnderlayImage.rotate = -0.015

# how many events to show in the log panel (default = 30)
GUI.EventLogPanel.nrofEvents = 100
# Regular Expression log filter (see Pattern-class from the Java API for RE-matching details)
#GUI.EventLogPanel.REfilter = .*p[1-9]<->p[1-9]$